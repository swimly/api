<?php 
header("Content-type: text/html; charset=utf-8"); 
require("conn.php");
$arr=array(
	'projects'=>"   
		id int(10) UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY, 
		thumb varchar(80) comment '缩略图',    
		title varchar(30) not null comment '项目名称', 
		url varchar(50) not null comment '项目链接',
		introduce varchar(120) not null comment '项目描述',
		type varchar(10) not null comment '项目类型',
		views int(4) default '0' comment '浏览次数',
		praise int(3) default '0' comment '赞',
		stamp int(3) default '0' comment '踩',
		resource varchar(30) comment '源码地址',
		secret int(1) default '0' not null comment '时候私密项目',
		recommend tinyint(1) not null default '0' comment '是否推荐', 
		time datetime NOT NULL comment '添加时间'
	",
	'article'=>"   
		id int(4) UNSIGNED AUTO_INCREMENT NOT NULL PRIMARY KEY, 
		thumb varchar(80) comment '缩略图',    
		title varchar(30) not null comment '项目名称', 
		url varchar(50) not null comment '原链接',
		content varchar(120) not null comment '文章内容',
		type varchar(10) not null comment '文章类型',
		views int(4) default '0' comment '浏览次数',
		assess int(3) default '0' comment '评价数',
		praise int(3) default '0' comment '赞',
		stamp int(3) default '0' comment '踩',
		keyword varchar(30) comment '关键字',
		author varchar(15) not null default 'admin' comment '作者', 
		time datetime NOT NULL comment '添加时间'
	",
);
if(!$conn){
	echo"数据库连接失败";
}else{
	function init($arr,$conn){
		foreach ($arr as $table=>$field) {
			$sql="CREATE TABLE IF NOT EXISTS ".$table."(".$field.")ENGINE=InnoDB DEFAULT CHARSET utf8 COLLATE utf8_general_ci;"; 
		    if(mysql_num_rows(mysql_query("show tables like'".$table."'"))==1){
		    	echo($table."存在<br/>");
		    }else if(mysql_query($sql,$conn)){
		    	echo($table."创建成功！<br/>");
		    }
			mysql_query("set names 'utf8'");
			mysql_query("SET CHARACTER SET UTF8"); 
			mysql_query("SET CHARACTER_SET_RESULTS=UTF8'");
			mysql_query($sql,$conn) or die(mysql_error());
		} 
	}
	init($arr,$conn);
}
?>